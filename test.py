import json
import requests

# Get
response = requests.get('https://interns.bcgdvsydney.com/api/v1/key')
get_result = json.loads(response.text)
key = get_result['key']

# Post
url = "https://interns.bcgdvsydney.com/api/v1/submit"
params = {'apiKey': key}
data = {'name': 'Vincent (Truong Vu Viet)', 'email': 'truongvv@gmail.com'}
headers = {'Content-type': 'application/json'}
r = requests.post(url, params=params, json=data, headers=headers)

print(r)


